
import { Breadcrumb } from "antd";

export default function Dashboard() {
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item>All Courses</Breadcrumb.Item>
        <Breadcrumb.Item>My Dashboard</Breadcrumb.Item>
      </Breadcrumb>
      <div>Greetings Shyam,</div>
      Your dashboard has list of all your enrolled courses
    </div>
  );
}