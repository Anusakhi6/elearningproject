import "./App.css";
import { Form, Input, Button } from "antd";
import Dashboard from "./dashboard";

function App() {
  return (
    <div className="container">
      <div className="heading">
        <p>
        Welcome to{" "}<br/>
        <span style={{ color: "orange", fontweight: "bold" }}>
          Crystal Delta<br/>
        </span>{" "}
        e-learning
        </p>      
      </div>
      <div
        className="App"
        style={{
          border: "1px solid black",
          padding: "20px",
          width: "400px",
          backgroundcolor: "red"
        }}
      >
        <div className="heading2">Login to your account</div>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
        >
          <Form.Item
            name="CustomerId"
            // rules={[
            //   { required: true, message: "Please input your CustomerId!" }
            // ]}
          >
            <div style={{ margin: "5px 0px 5px 0px" }}>
              <label>Customer Id</label>
            </div>
            <Input
              style={{ margin: "10px", padding: "10px", width: "250px" }}
              placeholder="CustomerId"
            />
          </Form.Item>
          <Form.Item
            name="password"
            // rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <label>Password</label>
            <div>
              <Input
                style={{ margin: "10px", padding: "10px", width: "250px" }}
                type="password"
                placeholder="Password"
              />
            </div>
          </Form.Item>
          <Form.Item>
            <Button
              style={{ margin: "10px", padding: "10px", width: "250px" }}
              type="danger"
              // htmlType="submit"
              className="login-form-button"
              onClick={()=><Dashboard />}
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
export default App;